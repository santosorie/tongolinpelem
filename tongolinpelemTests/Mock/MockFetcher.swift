//
//  MockFetcher.swift
//  tongolinpelemTests
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
@testable import tongolinpelem

class MockFetcher: Fetcher {
    var mockGenreList: GenreList!
    var mockMovieResponse: MovieResponse!
    
    var isStimulateSuccess: Bool = false
    var errorMessage: String = ""
    
    override init() { }
    
    override func retrieveGenres(onSuccess: @escaping (GenreList) -> Void, onFailure: @escaping (String) -> Void) {
        if isStimulateSuccess {
            onSuccess(mockGenreList)
        } else {
            onFailure(errorMessage)
        }
    }
    
    override func retrieveMovies(by genreId: Int, onSuccess: @escaping (MovieResponse) -> Void, onFailure: @escaping (String) -> Void) {
        if isStimulateSuccess {
            onSuccess(mockMovieResponse)
        } else {
            onFailure(errorMessage)
        }
    }
}
