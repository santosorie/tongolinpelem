//
//  GenreViewModelTests.swift
//  GenreViewModelTests
//
//  Created by Harrie Santoso on 09/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import tongolinpelem

class GenreViewModelTests: XCTestCase {

    var mockFetcher: MockFetcher!
    var genreProvider: GenreProvider!
    var genreViewModel: GenreViewModel!
    let decoder = JSONDecoder()
    
    override func setUp() {
        super.setUp()
        genreProvider = GenreProvider()
        mockFetcher = MockFetcher()
        genreViewModel = GenreViewModel(genreProvider: genreProvider, fetcher: mockFetcher)
    }

    func testSuccessFetchGenres() {
        mockFetcher.isStimulateSuccess = true
        let randomNumber: Int = Int.random(in: 1...100)
        let data = Data("""
                        {
                        "genres": [
                            {
                                "id": \(randomNumber),
                                "name": "Action"
                            },
                            {
                                "id": 12,
                                "name": "Adventure"
                            }]
                        }
                        """.utf8)
        
        do {
            let genreList = try decoder.decode(GenreList.self, from: data)
            mockFetcher.mockGenreList = genreList
        } catch {
            XCTFail("bad json given")
        }
        
        genreViewModel.fetchGenres()
        
        XCTAssertEqual(genreViewModel.genreProvider.genres.first!.id, randomNumber)
    }
    
    func testFailFetchGenres() {
        mockFetcher.isStimulateSuccess = false
        let randomNumber: Int = Int.random(in: 1...100)
        let errorMessage: String = "ini error message dengan angka: \(randomNumber)"
        mockFetcher.errorMessage = errorMessage
        
        genreViewModel.fetchGenres()
        
        XCTAssertEqual(genreViewModel.errorMessage, errorMessage)
    }
}
