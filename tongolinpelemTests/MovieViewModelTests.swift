//
//  MovieViewModelTests.swift
//  tongolinpelemTests
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import XCTest
@testable import tongolinpelem

class MovieViewModelTests: XCTestCase {
    
    var mockFetcher: MockFetcher!
    var movieProvider: MovieProvider!
    var movieViewModel: MovieViewModel!
    let decoder = JSONDecoder()
    
    override func setUp() {
        super.setUp()
        movieProvider = MovieProvider()
        mockFetcher = MockFetcher()
        movieViewModel = MovieViewModel(movieProvider: movieProvider, fetcher: mockFetcher)
    }

    func testSuccessFetchMovies() {
        mockFetcher.isStimulateSuccess = true
        let (randomNumber): Int = Int.random(in: 1...100)
        let data = Data("""
                        {
                        "page": 1,
                        "total_results": 1000,
                        "total_pages": 500,
                        "results": [
                          {
                            "id": \(randomNumber),
                            "title": "Ad Astra",
                            "release_date": "2019-09-17"
                          }]
                        }
                        """.utf8)
        
        do {
            let movieResponse = try decoder.decode(MovieResponse.self, from: data)
            mockFetcher.mockMovieResponse = movieResponse
        } catch {
            XCTFail("bad json given")
        }
        
        movieViewModel.fetchMovie(by: 0)
        
        XCTAssertEqual(movieViewModel.movieProvider.movies.first!.id, randomNumber)
    }
    
    func testFailFetchMovies() {
        mockFetcher.isStimulateSuccess = false
        let randomNumber: Int = Int.random(in: 1...100)
        let errorMessage: String = "ini error message dengan angka: \(randomNumber)"
        mockFetcher.errorMessage = errorMessage
        
        movieViewModel.fetchMovie(by: 0)
        
        XCTAssertEqual(movieViewModel.errorMessage, errorMessage)
    }
}
