//
//  MovieViewModel.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class MovieViewModel {
    
    var movieProvider: MovieProvider
    var errorMessage: String = ""
    
    private(set) var fetcher: Fetcher = .shared
    
    init(movieProvider: MovieProvider,
         fetcher: Fetcher = .shared) {
        self.movieProvider = movieProvider
        self.fetcher = fetcher
    }
    
    func fetchMovie(by genreId: Int,
                    page: Int = 1) {
        fetcher.retrieveMovies(by: genreId,
                               page:  page,
                               onSuccess: { [unowned self] movieResponse in
                                if movieResponse.page == 1 {
                                    self.movieProvider.movies = movieResponse.results
                                } else {
                                    self.movieProvider.movies.append(contentsOf: movieResponse.results)
                                }
                                
                                self.movieProvider.currentPage = movieResponse.page
                                },
                               onFailure: { [unowned self] errorMessage in
                                self.errorMessage = errorMessage
            
        })
    }
}
