//
//  GenreViewModel.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

class GenreViewModel {
    
    private(set) var fetcher: Fetcher = .shared
    var genreProvider: GenreProvider
    var errorMessage: String = ""
    
    init(genreProvider: GenreProvider,
         fetcher: Fetcher = .shared) {
        self.genreProvider = genreProvider
        self.fetcher = fetcher
    }
    
    func fetchGenres() {
        fetcher.retrieveGenres(onSuccess: { [unowned self] genreList in
            self.genreProvider.genres = genreList.genres
        }, onFailure: { [unowned self] errorMessage in
            self.errorMessage = errorMessage
        })
    }
}
