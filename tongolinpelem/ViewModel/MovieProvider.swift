//
//  MovieProvider.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class MovieProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    var notifChanges: (() -> Void)?
    var needLoadmoreFromPage: ((Int) -> Void)?
    
    var onTapMovie:  ((Movie) -> Void)?
    var currentPage: Int = 0
    
    enum Sections: Int {
        case movieList
        case loading
        case count
    }
    
    var movies: [Movie] = [] {
        didSet {
            notifChanges?()
        }
    }
    
    // MARK:- Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Sections.count.rawValue
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Sections.movieList.rawValue:
            return movies.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case Sections.movieList.rawValue:
            guard indexPath.row < movies.count else {
                return UICollectionViewCell()
            }
            
            return movieCell(collectionView, cellForItemAt: indexPath)
        default:
            return loadMoreCell(collectionView, cellForItemAt: indexPath)
        }
    }
    
    private func movieCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> MovieCell {
        let movie: Movie = movies[indexPath.row]
        let cell: MovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MovieCell.self),
                                                                 for: indexPath) as! MovieCell
        cell.titleText = movie.title
        cell.backgroundColor = .cyan
        
        return cell
    }
    
    private func loadMoreCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> LoadMoreCell {
        let cell: LoadMoreCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LoadMoreCell.self),
                                                                    for: indexPath) as! LoadMoreCell
        cell.backgroundColor = .yellow
        return cell
    }
    
    //MARK:- Delegate
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if currentPage > 0, indexPath.section == Sections.loading.rawValue {
            needLoadmoreFromPage?(currentPage)
        }
    }
}
