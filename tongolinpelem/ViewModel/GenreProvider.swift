//
//  GenreProvider.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class GenreProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    var notifChanges: (() -> Void)?
    var onTapGenre:  ((Genre) -> Void)?
    
    var genres: [Genre] = [] {
        didSet {
            notifChanges?()
        }
    }
    
    // MARK:- Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genres.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.row < genres.count else {
            return UICollectionViewCell()
        }
        let genre: Genre = genres[indexPath.row]
        let cell: GenreCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GenreCell.self),
                                                                 for: indexPath) as! GenreCell
        cell.titleText = genre.name
        
        return cell
    }
    
    // MARK:- Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row < genres.count else {
            return
        }
        
        let genre: Genre = genres[indexPath.row]
        onTapGenre?(genre)
    }
}
