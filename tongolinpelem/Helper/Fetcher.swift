//
//  Fetcher.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation
import Alamofire

class Fetcher {
    static let shared = Fetcher()
    
    private let apiKey: String = "api_key"
    private let apiKeyValue: String = "d0e5b170ee60aa4ab5add891020ebf83"
    private let baseUrl: String = "https://api.themoviedb.org/3/"
    
    init() { }
    
    func retrieveGenres(onSuccess: @escaping (GenreList) -> Void, onFailure: @escaping (String)-> Void) {
        let getGenresUrl: String = baseUrl + "genre/movie/list"
        let params: Parameters = [apiKey: apiKeyValue]
        AF.request(getGenresUrl, parameters: params).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let genres = try JSONDecoder().decode(GenreList.self, from: data)
                        onSuccess(genres)
                    } else {
                        onFailure("fail unwrap data")
                    }
                } catch {
                    onFailure("error parsing json")
                }
            case .failure(let error):
                onFailure(error.localizedDescription)
            }
        }
    }
    
    func retrieveMovies(by genreId: Int,
                        page: Int = 1,
                        onSuccess: @escaping (MovieResponse) -> Void,
                        onFailure: @escaping (String)-> Void) {
        let getMoviesUrl: String = baseUrl + "discover/movie"
        let params: Parameters = [apiKey: apiKeyValue,
                                  "page": page,
                                  "with_genres": genreId]
        AF.request(getMoviesUrl, parameters: params).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                do {
                    if let data = response.data {
                        let movies = try JSONDecoder().decode(MovieResponse.self, from: data)
                        onSuccess(movies)
                    } else {
                        onFailure("fail unwrap data")
                    }
                    
                } catch let error {
                    onFailure("error parsing json: \(error)")
                }
            case .failure(let error):
                onFailure(error.localizedDescription)
            }
        }
    }
}
