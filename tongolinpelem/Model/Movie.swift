//
//  Movie.swift
//  tongolinPelem
//
//  Created by Harrie Santoso on 09/04/20.
//

import Foundation

struct Movie: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case releaseDate = "release_date"
    }
    
    var id: Int
    var title: String
    var releaseDate: String
}

struct MovieResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
    
    var page: Int
    var totalResults: Int
    var totalPages: Int
    var results: [Movie]
}
