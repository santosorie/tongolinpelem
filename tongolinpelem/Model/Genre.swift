//
//  Genre.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 09/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import Foundation

struct Genre: Decodable {
    var id: Int
    var name: String
}

struct GenreList: Decodable {
    let genres: [Genre]
    
    private enum CodingKeys: CodingKey {
        case genres
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let data = try values.decode([Genre].self, forKey: .genres)
        genres = data
    }
}
