//
//  ViewController.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 09/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private lazy var genreShowcase: UICollectionView = {
        let cv: UICollectionView = UICollectionView(frame: .zero,
                                                    collectionViewLayout: makeGenreLayout())
        
        return cv
    }()
    
    private func makeGenreLayout() -> UICollectionViewFlowLayout {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 180, height: 60)
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        return layout
    }
    private let genreProvider: GenreProvider = GenreProvider()
    private lazy var genreViewModel: GenreViewModel = {
        let viewModel: GenreViewModel = GenreViewModel(genreProvider: genreProvider)
        return viewModel
    }()
    
    private lazy var movieGrid: UICollectionView = {
        let cv: UICollectionView = UICollectionView(frame: .zero,
                                                    collectionViewLayout: makeMovieLayout())
        return cv
    }()
    
    private func makeMovieLayout() -> UICollectionViewFlowLayout {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let itemWidth: CGFloat = view.frame.width * 0.4
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        layout.scrollDirection = .vertical
        
        return layout
    }
    
    private let movieProvider: MovieProvider = MovieProvider()
    private lazy var movieViewModel: MovieViewModel = {
        let viewModel: MovieViewModel = MovieViewModel(movieProvider: movieProvider)
        return viewModel
    }()
    
    private var tappedGenreId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGenreShowcase()
        setupMovieGrid()
        genreViewModel.fetchGenres()
    }
    
    private func setupGenreShowcase() {
        genreProvider.notifChanges = { [unowned self] in
            self.genreShowcase.reloadData()
        }
        genreProvider.onTapGenre = { [unowned self] genre in
            self.tappedGenreId = genre.id
            self.movieViewModel.fetchMovie(by: self.tappedGenreId)
        }
        
        genreShowcase.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 150)
        genreShowcase.backgroundColor = .green
        genreShowcase.dataSource = genreProvider
        genreShowcase.delegate = genreProvider
        genreShowcase.register(GenreCell.self, forCellWithReuseIdentifier: String(describing: GenreCell.self))
        view.addSubview(genreShowcase)
    }
    
    private func setupMovieGrid() {
        movieProvider.notifChanges = { [unowned self] in
            self.movieGrid.reloadData()
        }
        movieProvider.needLoadmoreFromPage = { [unowned self] page in
            self.movieViewModel.fetchMovie(by: self.tappedGenreId, page: page + 1)
        }
        movieProvider.onTapMovie = { movie in
            // TODO: navigate to movie detail
        }
        
        movieGrid.frame = CGRect(x: 0,
                                 y: genreShowcase.frame.height,
                                 width: view.frame.width,
                                 height: view.frame.height - genreShowcase.frame.height)
        
        movieGrid.backgroundColor = .blue
        movieGrid.dataSource = movieProvider
        movieGrid.delegate = movieProvider
        movieGrid.register(MovieCell.self, forCellWithReuseIdentifier: String(describing: MovieCell.self))
        movieGrid.register(LoadMoreCell.self, forCellWithReuseIdentifier: String(describing: LoadMoreCell.self))
        view.addSubview(movieGrid)
    }
}

