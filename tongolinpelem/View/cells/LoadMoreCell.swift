//
//  LoadMoreCell.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class LoadMoreCell: UICollectionViewCell {
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .medium)
        indicator.startAnimating()
        indicator.frame.size = CGSize(width: 80, height: 80)
        indicator.center = contentView.center
        
        contentView.backgroundColor = .black
        
        contentView.addSubview(indicator)
        return indicator
    }()
}
