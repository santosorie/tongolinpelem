//
//  MovieCell.swift
//  tongolinpelem
//
//  Created by Harrie Santoso on 10/04/20.
//  Copyright © 2020 Harrie. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    var titleText: String = "" {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.frame.size = CGSize(width: contentView.frame.width,
                                  height: 24)
        label.center = contentView.center
        label.textAlignment = .center
        contentView.addSubview(label)
        
        return label
    }()
}
